﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Abstractions
{
    public interface ICustomerService
    {
       public Task<List<CustomerShortResponse>> GetCustomersAsync();
       public Task<CustomerResponse> GetCustomerAsync(Guid id);
       public Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request);
       public Task<bool> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request);
       public Task<bool> DeleteCustomerAsync(Guid id);
    }
}
